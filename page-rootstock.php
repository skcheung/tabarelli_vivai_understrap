<?php
/*
      Template Name: Rootstock Page
 */

get_header();

 //===============
 // Custom Fields
 //===============
 // Rootstock Section
$presentation_section_title = get_field('presentation_section_title');
$presentation_section_text = get_field('presentation_section_text');
$apples_section_title = get_field('apples_section_title');
$apples_section_text = get_field('apples_section_text');
$pears_section_title = get_field('pears_section_title');
$pears_section_text = get_field('pears_section_text');

?>

<!-- Hero Section -->
  <section id="hero-section-porte-greffes">
    <div class="layer">
      <div class="container">
        <h1>Sample Title</h1>
        <p class="lead">Lorem ipsum dolor sit amet</p>
      </div>
    </div>
  </section>

  <!-- Tabs Section -->
  <section id="tabs-section">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <h2><?php the_title(); ?></h2>
          <ul class="nav flex-column">
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link active" href="#presentation" role="tab"><?php echo $presentation_section_title; ?></a>
            </li>
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link" href="#pommes" role="tab"><?php echo $apples_section_title; ?></a>
            </li>
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link" href="#poires" role="tab"><?php echo $pears_section_title; ?></a>
            </li>
          </ul>
        </div>
          <!-- Tabs Content -->
          <div class="tab-content col-sm-8">
            <div class="tab-pane fade show active" id="presentation">
              <h3><?php echo $presentation_section_title; ?></h3>
              <?php echo $presentation_section_text; ?>
            </div>
            <div class="tab-pane fade" id="pommes">
              <h3><?php echo $apples_section_title; ?></h3>
              <?php echo $apples_section_text; ?>
            </div>
            <div class="tab-pane fade" id="poires">
              <h3><?php echo $pears_section_title; ?></h3>
              <?php echo $pears_section_text; ?>
            </div>
        </div>
      </div>
    </div>
  </section>

<?php
get_footer();