<?php
/*
      Template Name: Apples Page
 */

get_header();

 //===============
 // Custom Fields
 //===============
 // GALA Section
$gala_1_title = get_field('gala_1_title');
$gala_1_description = get_field('gala_1_description');
$gala_1_image = get_field('gala_1_image');
$gala_1_content = get_field('gala_1_content');
$gala_2_title = get_field('gala_2_title');
$gala_2_description = get_field('gala_2_description');
$gala_2_image = get_field('gala_2_image');
$gala_2_content = get_field('gala_2_content');
$gala_3_title = get_field('gala_3_title');
$gala_3_description = get_field('gala_3_description');
$gala_3_image = get_field('gala_3_image');
$gala_3_content = get_field('gala_3_content');
$gala_4_title = get_field('gala_4_title');
$gala_4_description = get_field('gala_4_description');
$gala_4_image = get_field('gala_4_image');
$gala_4_content = get_field('gala_4_content');

// GOLDEN Section
$golden_1_title = get_field('golden_1_title');
$golden_1_description = get_field('golden_1_description');
$golden_1_image = get_field('golden_1_image');
$golden_1_content = get_field('golden_1_content');
$golden_2_title = get_field('golden_2_title');
$golden_2_description = get_field('golden_2_description');
$golden_2_image = get_field('golden_2_image');
$golden_2_content = get_field('golden_2_content');
$golden_3_title = get_field('golden_3_title');
$golden_3_description = get_field('golden_3_description');
$golden_3_image = get_field('golden_3_image');
$golden_3_content = get_field('golden_3_content');
$golden_4_title = get_field('golden_4_title');
$golden_4_description = get_field('golden_4_description');
$golden_4_image = get_field('golden_4_image');
$golden_4_content = get_field('golden_4_content');

// RED Section
$red_1_title = get_field('red_1_title');
$red_1_description = get_field('red_1_description');
$red_1_image = get_field('red_1_image');
$red_1_content = get_field('red_1_content');
$red_2_title = get_field('red_2_title');
$red_2_description = get_field('red_2_description');
$red_2_image = get_field('red_2_image');
$red_2_content = get_field('red_2_content');
$red_3_title = get_field('red_3_title');
$red_3_description = get_field('red_3_description');
$red_3_image = get_field('red_3_image');
$red_3_content = get_field('red_3_content');
$red_4_title = get_field('red_4_title');
$red_4_description = get_field('red_4_description');
$red_4_image = get_field('red_4_image');
$red_4_content = get_field('red_4_content');
$red_5_title = get_field('red_5_title');
$red_5_description = get_field('red_5_description');
$red_5_image = get_field('red_5_image');
$red_5_content = get_field('red_5_content');

// RESISTENTI Section
$resistenti_1_title = get_field('resistenti_1_title');
$resistenti_1_description = get_field('resistenti_1_description');
$resistenti_1_image = get_field('resistenti_1_image');
$resistenti_1_content = get_field('resistenti_1_content');
$resistenti_2_title = get_field('resistenti_2_title');
$resistenti_2_description = get_field('resistenti_2_description');
$resistenti_2_image = get_field('resistenti_2_image');
$resistenti_2_content = get_field('resistenti_2_content');
$resistenti_3_title = get_field('resistenti_3_title');
$resistenti_3_description = get_field('resistenti_3_description');
$resistenti_3_image = get_field('resistenti_3_image');
$resistenti_3_content = get_field('resistenti_3_content');
$resistenti_4_title = get_field('resistenti_4_title');
$resistenti_4_description = get_field('resistenti_4_description');
$resistenti_4_image = get_field('resistenti_4_image');
$resistenti_4_content = get_field('resistenti_4_content');

// VARIE Section
$varie_1_title = get_field('varie_1_title');
$varie_1_description = get_field('varie_1_description');
$varie_1_image = get_field('varie_1_image');
$varie_1_content = get_field('varie_1_content');
$varie_2_title = get_field('varie_2_title');
$varie_2_description = get_field('varie_2_description');
$varie_2_image = get_field('varie_2_image');
$varie_2_content = get_field('varie_2_content');
$varie_3_title = get_field('varie_3_title');
$varie_3_description = get_field('varie_3_description');
$varie_3_image = get_field('varie_3_image');
$varie_3_content = get_field('varie_3_content');
$varie_4_title = get_field('varie_4_title');
$varie_4_description = get_field('varie_4_description');
$varie_4_image = get_field('varie_4_image');
$varie_4_content = get_field('varie_4_content');
$varie_5_title = get_field('varie_5_title');
$varie_5_description = get_field('varie_5_description');
$varie_5_image = get_field('varie_5_image');
$varie_5_content = get_field('varie_5_content');

// Buttons
$read_more = get_field('read_more');

?>

<!-- Hero Section -->
  <section id="hero-section-apples">
    <div class="layer">
      <div class="container">
        <h1>Sample Title</h1>
        <p class="lead">Lorem ipsum dolor sit amet</p>
      </div>
    </div>
  </section>

  <!-- Tabs Section -->
  <section id="tabs-section">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <h2><?php the_title(); ?></h2>
          <ul class="nav flex-column">
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link active" href="#gala" role="tab">GALA</a>
            </li>
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link" href="#golden" role="tab">GOLDEN</a>
            </li>
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link" href="#red" role="tab">RED</a>
            </li>
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link" href="#resistenti" role="tab">RESISTENTI</a>
            </li>
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link" href="#varie" role="tab">VARIE</a>
            </li>
          </ul>
        </div>
          <!-- Tabs Content -->
          <div class="tab-content col-sm-8" id="variety">
            <div class="tab-pane fade show active" id="gala">
              <h3>GALA</h3>
              <div class="container">
                <div class="row">
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $gala_1_image['url']; ?>" alt="<?php echo $gala_1_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $gala_1_title; ?></h4>
                        <p class="card-text"><?php echo $gala_1_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#schnico"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $gala_2_image['url']; ?>" alt="<?php echo $gala_2_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $gala_2_title; ?></h4>
                        <p class="card-text"><?php echo $gala_2_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#schnicoRed"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div>
                </div><!-- .row -->
                </div><!-- .container -->
              </div><!-- .tab-pane -->

            <div class="tab-pane fade" id="golden">
              <h3>GOLDEN</h3>
              <div class="container">
                <div class="row">
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $golden_1_image['url']; ?>" alt="<?php echo $golden_1_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $golden_1_title; ?></h4>
                        <p class="card-text"><?php echo $golden_1_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#goldenCLB"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div><!-- .col -->
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $golden_2_image['url']; ?>" alt="<?php echo $golden_2_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $golden_2_title; ?></h4>
                        <p class="card-text"><?php echo $golden_2_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#goldenCLReinders"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $golden_3_image['url']; ?>" alt="<?php echo $golden_3_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $golden_3_title; ?></h4>
                        <p class="card-text"><?php echo $golden_3_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#goldenCLSmoothee"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                        <img class="card-img-top" src="<?php echo $golden_4_image['url']; ?>" alt="<?php echo $golden_4_image['alt']; ?>">
                        <div class="card-block">
                            <h4 class="card-title"><?php echo $golden_4_title; ?></h4>
                            <p class="card-text"><?php echo $golden_4_description; ?></p>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#goldenOrange"><?php echo $read_more; ?> &raquo;</button>
                        </div>
                    </div>
                  </div>
                </div><!-- .row -->
              </div><!-- .container -->
            </div>

            <div class="tab-pane fade" id="red">
              <h3>RED</h3>
              <div class="container">
                <div class="row">
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $red_1_image['url']; ?>" alt="<?php echo $red_1_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $red_1_title; ?></h4>
                        <p class="card-text"><?php echo $red_1_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#superChief"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div><!-- .col -->
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $red_2_image['url']; ?>" alt="<?php echo $red_2_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $red_2_title; ?></h4>
                        <p class="card-text"><?php echo $red_2_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#redChief"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $red_3_image['url']; ?>" alt="<?php echo $red_3_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $red_3_title; ?></h4>
                        <p class="card-text"><?php echo $red_3_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#spurEvasni"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                        <img class="card-img-top" src="<?php echo $red_4_image['url']; ?>" alt="<?php echo $red_4_image['alt']; ?>">
                        <div class="card-block">
                            <h4 class="card-title"><?php echo $red_4_title; ?></h4>
                            <p class="card-text"><?php echo $red_4_description; ?></p>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#redVelox"><?php echo $read_more; ?> &raquo;</button>
                        </div>
                    </div>
                  </div>
                </div><!-- .row -->
              </div><!-- .container -->
            </div>
            
            <div class="tab-pane fade" id="resistenti">
              <h3>RESISTENTI</h3>
              <div class="container">
                <div class="row">
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $resistenti_1_image['url']; ?>" alt="<?php echo $resistenti_1_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $resistenti_1_title; ?></h4>
                        <p class="card-text"><?php echo $resistenti_1_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#redFree"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div><!-- .col -->
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $resistenti_2_image['url']; ?>" alt="<?php echo $resistenti_2_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $resistenti_2_title; ?></h4>
                        <p class="card-text"><?php echo $resistenti_2_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#goldenOrange"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $resistenti_3_image['url']; ?>" alt="<?php echo $resistenti_3_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $resistenti_3_title; ?></h4>
                        <p class="card-text"><?php echo $resistenti_3_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#topaz"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div>
                </div><!-- .row -->
              </div><!-- .container -->
            </div>

            <div class="tab-pane fade" id="varie">
              <h3>VARIE</h3>
              <div class="container">
                <div class="row">
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $varie_1_image['url']; ?>" alt="<?php echo $varie_1_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $varie_1_title; ?></h4>
                        <p class="card-text"><?php echo $varie_1_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#fujiF12"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div><!-- .col -->
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $varie_2_image['url']; ?>" alt="<?php echo $varie_2_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $varie_2_title; ?></h4>
                        <p class="card-text"><?php echo $varie_2_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#grannySmith"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $varie_3_image['url']; ?>" alt="<?php echo $varie_3_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $varie_3_title; ?></h4>
                        <p class="card-text"><?php echo $varie_3_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#dallago"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $varie_4_image['url']; ?>" alt="<?php echo $varie_4_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $varie_4_title; ?></h4>
                        <p class="card-text"><?php echo $varie_4_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#canadaBianca"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-lg-6">
                    <div class="card">
                      <img class="card-img-top" src="<?php echo $varie_5_image['url']; ?>" alt="<?php echo $varie_5_image['alt']; ?>">
                      <div class="card-block">
                        <h4 class="card-title"><?php echo $varie_5_title; ?></h4>
                        <p class="card-text"><?php echo $varie_5_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#redRome"><?php echo $read_more; ?> &raquo;</button>
                      </div>
                    </div>
                  </div>
                </div><!-- .row -->
              </div><!-- .container -->
            </div>

        </div>
      </div>
    </div>
  </section>

  <!-- Modal GALA -->
  <div id="schnico" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $gala_1_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php echo $gala_1_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="schnicoRed" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $gala_2_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php echo $gala_2_content; ?>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal GOLDEN -->
  <div id="goldenCLB" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $golden_1_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $golden_1_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="goldenCLReinders" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $golden_2_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $golden_2_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="goldenCLSmoothee" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $golden_3_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $golden_3_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="goldenOrange" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal Content -->
            <div class="modal-content">
                <div class="modal-header">
                    <h5><?php echo $golden_4_title; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo $golden_4_content; ?>
                </div>
            </div>
        </div>
  </div>

  <!-- Modal RED -->
  <div id="superChief" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $red_1_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $red_1_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="redChief" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $red_2_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $red_2_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="spurEvasni" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $red_3_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $red_3_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="redVelox" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal Content -->
            <div class="modal-content">
                <div class="modal-header">
                    <h5><?php echo $red_4_title; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo $red_4_content; ?>
                </div>
            </div>
        </div>
  </div>

  <!-- Modal RESISTENTI -->
  <div id="redFree" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $resistenti_1_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $resistenti_1_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="goldenOrange" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $resistenti_2_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $resistenti_2_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="topaz" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $resistenti_3_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $resistenti_3_content; ?>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal VARIE -->
  <div id="fujiF12" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $varie_1_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $varie_1_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="grannySmith" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $varie_2_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $varie_2_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="dallago" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $varie_3_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $varie_3_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="canadaBianca" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $varie_4_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $varie_4_content; ?>
        </div>
      </div>
    </div>
  </div>
  <div id="redRome" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal Content -->
      <div class="modal-content">
        <div class="modal-header">
          <h5><?php echo $varie_5_title; ?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <?php echo $varie_5_content; ?>
        </div>
      </div>
    </div>
  </div>

<?php
get_footer();