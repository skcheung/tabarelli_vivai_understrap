<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <link rel="icon" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon.ico">
	<?php wp_head(); ?>
  <title>
  <?php 
    if(is_home()) {
      echo bloginfo("name");
      echo " | ";
      echo bloginfo("description");
    } else {
      echo wp_title(" | ", false, right);
      echo bloginfo("name");
    }
  ?>
  </title>
</head>

<body <?php body_class(); ?>>

<!-- Header -->
  <header id="header">
    <div class="container">
      <div class="row justify-content-end"> 
        <div class="mr-auto p-2" id="logo-container">
          <a href="/"><img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/Marchio Tabarelli.svg" alt="Logo de Tabarelli Vivai" id="logo"></a>
        </div>
        <!--<div class="p-2" id="searchBar">
          <form method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" class="form-inline" role="search">
            <input type="text" name="s" id="searchInput" placeholder="Search...">
            <button class="btn btn-primary input-group-addon"><i class="fa fa-search"></i></button>
          </form>-->
          <?php dynamic_sidebar( 'headerright' ); ?>
        </div>
      </div>
    </div>
  </div>

    <!-- Navigation -->
      <nav class="navbar navbar-toggleable-lg navbar-inverse bg-inverse">
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="container">
          	<a class="navbar-brand" href="/">Tabarelli Vivai</a>
						<!-- The WordPress Menu goes here -->
						<?php wp_nav_menu(
							array(
								'theme_location'  => 'primary',
								'container'				=> 'div',
								'container_class' => 'collapse navbar-collapse',
								'container_id'    => 'navbarText',
								'menu_class'      => 'navbar-nav',
								'fallback_cb'     => '',
								'menu_id'         => 'main-menu',
								'walker'          => new WP_Bootstrap_Navwalker(),
							)
						); ?>
          </div>
      </nav>
  </header>
