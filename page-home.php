<?php
/*
      Template Name: Home Page
 */
 //===============
 // Custom Fields
 //===============
 // Hero Slider
$hero_slider_1_title = get_field('hero_slider_1_title');
$hero_slider_1_desc  = get_field('hero_slider_1_desc');
$hero_slider_1_img   = get_field('hero_slider_1_img');
$hero_slider_2_title = get_field('hero_slider_2_title');
$hero_slider_2_desc  = get_field('hero_slider_2_desc');
$hero_slider_2_img   = get_field('hero_slider_2_img');
$hero_slider_3_title = get_field('hero_slider_3_title');
$hero_slider_3_desc  = get_field('hero_slider_3_desc');
$hero_slider_3_img   = get_field('hero_slider_3_img');

  // Who Are We Section
$who_are_we_section_title = get_field('who_are_we_section_title');
$who_are_we_section_text = get_field('who_are_we_section_text');
$who_are_we_section_link = get_field('who_are_we_section_link');
$who_are_we_section_button = get_field('who_are_we_section_button');

// Our Products Section
$our_products_section_title = get_field('our_products_section_title');
$our_products_section_text = get_field('our_products_section_text');
$our_products_section_link = get_field('our_products_section_link');
$our_products_section_button = get_field('our_products_section_button');

get_header();

?>

<!-- Hero Slider -->
  <section id="hero-slider">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
          <img class="d-block img-fluid" src="<?php echo $hero_slider_1_img['url']; ?>" alt="<?php echo $hero_slider_1_img['alt']; ?>">
          <div class="carousel-caption d-sm-block">
            <h2><?php echo $hero_slider_1_title ?></h2>
            <p class="lead"><?php echo $hero_slider_1_desc ?></p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block img-fluid" src="<?php echo $hero_slider_2_img['url']; ?>" alt="<?php echo $hero_slider_2_img['alt']; ?>">
          <div class="carousel-caption d-sm-block">
            <h2><?php echo $hero_slider_2_title ?></h2>
            <p class="lead"><?php echo $hero_slider_2_desc ?></p>
          </div>
        </div>
        <div class="carousel-item">
          <img class="d-block img-fluid" src="<?php echo $hero_slider_3_img['url']; ?>" alt="<?php echo $hero_slider_3_img['alt']; ?>">
          <div class="carousel-caption d-sm-block">
            <h2><?php echo $hero_slider_3_title ?></h2>
            <p class="lead"><?php echo $hero_slider_3_desc ?></p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" rel="noindex">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" rel="noindex">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </section>

  <!-- Who Are We Section -->
  <section id="company-section">
    <div class="container">
      <h2><?php echo $who_are_we_section_title; ?></h2>
      <p><?php echo $who_are_we_section_text; ?></p>
      <a href="<?php echo $who_are_we_section_link; ?>"><?php echo $who_are_we_section_button; ?> <i class="fa fa-chevron-circle-right "></i></a>
    </div>
  </section>

  <!-- Our Product Section -->
  <section id="products-section">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 offset-sm-2">
          <h2><?php echo $our_products_section_title; ?></h2>
          <p><?php echo $our_products_section_text; ?></p>
          <a href="<?php echo $our_products_section_link; ?>"><?php echo $our_products_section_button; ?> <i class="fa fa-chevron-circle-right "></i></a>
        </div>
      </div>
    </div>
  </section>

  <!-- Section 3 -->
  <!-- <section id="section-3">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <h2>Content Element 1</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nisi nibh, porta at vehicula nec, condimentum vel nulla. Vestibulum id nisl fringilla, commodo magna non, sagittis purus.</p>
          <a href="#">Lire plus <i class="fa fa-chevron-circle-right "></i></a>
        </div>
        <div class="col-sm-4">
          <h2>Content Element 2</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nisi nibh, porta at vehicula nec, condimentum vel nulla. Vestibulum id nisl fringilla, commodo magna non, sagittis purus.</p>
          <a href="#">Lire plus <i class="fa fa-chevron-circle-right "></i></a>
        </div>
        <div class="col-sm-4">
          <h2>Content Element 3</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nisi nibh, porta at vehicula nec, condimentum vel nulla. Vestibulum id nisl fringilla, commodo magna non, sagittis purus.</p>
          <a href="#">Lire plus <i class="fa fa-chevron-circle-right "></i></a>
        </div>
      </div>
    </div>
  </section> -->

  <?php
  get_footer();