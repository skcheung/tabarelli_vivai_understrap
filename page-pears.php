<?php
/*
      Template Name: Pears Page
 */

get_header();

//==============
// Custom Fields
//==============
// Pears
$pear_1_title = get_field('pear_1_title');
$pear_1_description = get_field('pear_1_description');
$pear_1_image = get_field('pear_1_image');
$pear_1_content = get_field('pear_1_content');
$pear_2_title = get_field('pear_2_title');
$pear_2_description = get_field('pear_2_description');
$pear_2_image = get_field('pear_2_image');
$pear_2_content = get_field('pear_2_content');
$pear_3_title = get_field('pear_3_title');
$pear_3_description = get_field('pear_3_description');
$pear_3_image = get_field('pear_3_image');
$pear_3_content = get_field('pear_3_content');
$pear_4_title = get_field('pear_4_title');
$pear_4_description = get_field('pear_4_description');
$pear_4_image = get_field('pear_4_image');
$pear_4_content = get_field('pear_4_content');
$pear_5_title = get_field('pear_5_title');
$pear_5_description = get_field('pear_5_description');
$pear_5_image = get_field('pear_5_image');
$pear_5_content = get_field('pear_5_content');


// Buttons
$read_more = get_field('read_more');

?>

    <!-- Hero Section -->
    <section id="hero-section-pears">
        <div class="layer">
            <div class="container">
                <h1>Sample Title</h1>
                <p class="lead">Lorem ipsum dolor sit amet</p>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <!-- Pears Cards -->
            <div class="col-sm-12 col-lg-6">
                <div class="card">
                    <img class="card-img-top" src="<?php echo $pear_1_image['url']; ?>" alt="<?php echo $pear_1_image['alt']; ?>">
                    <div class="card-block">
                        <h4 class="card-title"><?php echo $pear_1_title; ?></h4>
                        <p class="card-text"><?php echo $pear_1_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#abate"><?php echo $read_more; ?> &raquo;</button>
                    </div>
                </div>
            </div><!-- .col -->
            <div class="col-sm-12 col-lg-6">
                <div class="card">
                    <img class="card-img-top" src="<?php echo $pear_2_image['url']; ?>" alt="<?php echo $pear_2_image['alt']; ?>">
                    <div class="card-block">
                        <h4 class="card-title"><?php echo $pear_2_title; ?></h4>
                        <p class="card-text"><?php echo $pear_2_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#conference"><?php echo $read_more; ?> &raquo;</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6">
                <div class="card">
                    <img class="card-img-top" src="<?php echo $pear_3_image['url']; ?>" alt="<?php echo $pear_3_image['alt']; ?>">
                    <div class="card-block">
                        <h4 class="card-title"><?php echo $pear_3_title; ?></h4>
                        <p class="card-text"><?php echo $pear_3_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#kaiser"><?php echo $read_more; ?> &raquo;</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6">
                <div class="card">
                    <img class="card-img-top" src="<?php echo $pear_4_image['url']; ?>" alt="<?php echo $pear_4_image['alt']; ?>">
                    <div class="card-block">
                        <h4 class="card-title"><?php echo $pear_4_title; ?></h4>
                        <p class="card-text"><?php echo $pear_4_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#whiteWilliams"><?php echo $read_more; ?> &raquo;</button>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6">
                <div class="card">
                    <img class="card-img-top" src="<?php echo $pear_5_image['url']; ?>" alt="<?php echo $pear_5_image['alt']; ?>">
                    <div class="card-block">
                        <h4 class="card-title"><?php echo $pear_5_title; ?></h4>
                        <p class="card-text"><?php echo $pear_5_description; ?></p>
                        <button class="btn btn-primary" data-toggle="modal" data-target="#redWilliams"><?php echo $read_more; ?> &raquo;</button>
                    </div>
                </div>
            </div>
        </div><!-- .row -->
    </div><!-- .container -->

    <!-- Modal Pears -->
    <div id="abate" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal Content -->
            <div class="modal-content">
                <div class="modal-header">
                    <h5><?php echo $pear_1_title; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo $pear_1_content; ?>
                </div>
            </div>
        </div>
    </div>
    <div id="conference" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal Content -->
            <div class="modal-content">
                <div class="modal-header">
                    <h5><?php echo $pear_2_title; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo $pear_2_content; ?>
                </div>
            </div>
        </div>
    </div>
    <div id="kaiser" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal Content -->
            <div class="modal-content">
                <div class="modal-header">
                    <h5><?php echo $pear_3_title; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo $pear_3_content; ?>
                </div>
            </div>
        </div>
    </div>
    <div id="whiteWilliams" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal Content -->
            <div class="modal-content">
                <div class="modal-header">
                    <h5><?php echo $pear_4_title; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo $pear_4_content; ?>
                </div>
            </div>
        </div>
    </div>
    <div id="redWilliams" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal Content -->
            <div class="modal-content">
                <div class="modal-header">
                    <h5><?php echo $pear_5_title; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo $pear_5_content; ?>
                </div>
            </div>
        </div>
    </div>



<?php

get_footer();
