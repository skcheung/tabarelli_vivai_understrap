<?php

/*
  Template Name: Contact Page
*/

get_header();

?>

<!-- Hero Section -->
<section id="hero-section-contact">
  <div class="layer">
      <div class="container">
        <h2>Sample Title</h2>
        <p class="lead">Lorem ipsum dolor sit amet</p>
      </div>
    </div>
</section>

<!-- Contact Section -->
<section class="container">
  <div class="row">
    <div class="col-md-5" id="contact">
      <h2>Contact</h2>
      <p><strong>Tabarelli Vivai s.s</strong></p> 
      <p><b>Email : </b><a href="mailto:tabarellivivai@libero.it">tabarellivivai@libero.it</a></p>

      <div class="address"><b>SEDE: </b>
        <p>Via Maso, Alfonso, 1C</p>
        <p> 38010 Nave S.Rocco (Tn)</p>
      </div>
      <div class="telNumber">Tél et Fax : <span>+39 0461-870447</span></div>

      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2762.8878787307103!2d11.103966707121876!3d46.17288759312517!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x20225178af8b54f3!2sTABARELLI+VIVAI+-+PIANTE+DA+FRUTTO+E+PORTAINNESTI!5e0!3m2!1sfr!2sfr!4v1491912425064" frameborder="0" style="border:0" allowfullscreen></iframe>

      <div class="address"><b>VIVAIO: </b>
        <p>Via Cesare Battisti, 24</p>
        <p>25040 Masi (Pd)</p>
      </div>
      <div class="telNumber">Tél et Fax : <span>+39 0425-594249</span></div>

      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d11259.755127071601!2d11.506808627786631!3d45.127576294814574!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x477f04645d1b3f75%3A0x8123c02d8709ae5d!2sTABARELLI+VIVAI+-+Sede+produttiva!5e0!3m2!1sfr!2sfr!4v1491912706869" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="col-md-7" id="contactForm">
      <?php echo do_shortcode('[contact-form-7 id="16" title="Formulaire de contact 1"]'); ?>
    </div>
  </div>
</section>

<?php 

get_footer();
