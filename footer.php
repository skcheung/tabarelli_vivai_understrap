<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>

<?php wp_footer(); ?>

<!-- Footer -->
  <footer id="footer">
    <div class="container">
      <div class="row justify-content-center">
        <!-- The WordPress Menu goes here -->
              <?php wp_nav_menu(
                array(
                  'theme_location'  => 'primary',
                  'container'		=> false,
                  'container_class' => '',
                  'container_id'    => 'nav-footer',
                  'menu_class'      => 'navbar-footer',
                  'fallback_cb'     => '',
                  'menu_id'         => 'nav-footer',
                  'walker'          => new WP_Bootstrap_Navwalker(),
                )
              ); ?>
        <!--<ul>
          <li><a href="index.html">Accueil</a></li>
          <li><a href="about.html">À Propos</a></li>
          <li><a href="rootstock.html">Porte-greffes</a></li>
          <li><a href="apple.html">Pommes</a></li>
          <li><a href="pear.html">Poires</a></li>
          <li><a href="news.html">Revue Presse</a></li>
          <li><a href="contact.html">Contact</a></li>
          <li><a href="toc.html">Mentions légales</a></li>
        </ul>-->
        </div>
        <div class="row justify-content-center">
          <div class="col-12">
            <p>Tabarelli Vivai &copy; 2017</p>
          </div>
        </div>
      </div>
    </div>
  </footer>

	<!-- Custom Scripts -->
	<script src="<?php bloginfo('template_directory')?>/assets/js/main.js"></script>
</body>

</html>
