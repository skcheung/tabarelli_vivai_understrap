<?php
/*
      Template Name: About Page
 */

get_header();

 //===============
 // Custom Fields
 //===============
 // Enterprise Section
$enterprise_section_title = get_field('enterprise_section_title');
$enterprise_section_text = get_field('enterprise_section_text');

?>

<!-- Hero Section -->
  <section id="hero-section-about">
    <div class="layer">
      <div class="container">
        <h1>Sample Title</h1>
        <p class="lead">Lorem ipsum dolor sit amet</p>
      </div>
    </div>
  </section>

  <!-- Tabs Section -->
  <section id="tabs-section">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <h2><?php the_title(); ?></h2>
          <ul class="nav flex-column">
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link active" href="#entreprise" role="tab"><?php echo $enterprise_section_title; ?></a>
            </li>
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link" href="#territoire" role="tab">Territoire</a>
            </li>
            <li class="nav-item">
              <a data-toggle="tab" class="nav-link" href="#certifications" role="tab">Certifications</a>
            </li>
          </ul>
        </div>
          <!-- Tabs Content -->
          <div class="tab-content col-sm-8">
            <div class="tab-pane fade show active" id="entreprise">
              <h3><?php echo $enterprise_section_title; ?></h3>
              <p><?php echo $enterprise_section_text; ?></p>
            </div>
            <div class="tab-pane fade" id="territoire">
              <h3>Territoire</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit amet arcu aliquet, molestie justo at, auctor nunc. Phasellus ligula ipsum, volutpat eget semper id, viverra eget nibh. Suspendisse luctus mattis cursus. Nam consectetur ante at nisl hendrerit gravida. Donec vehicula rhoncus mattis. Mauris dignissim semper mattis. Fusce porttitor a mi at suscipit. Praesent facilisis dolor sapien, vel sodales augue mollis ut. Mauris venenatis magna eu tortor posuere luctus. Aenean tincidunt turpis sed dui aliquam vehicula. Praesent nec elit non dolor consectetur tincidunt sed in felis. Donec elementum, lacus at mattis tincidunt, eros magna faucibus sem, in condimentum est augue tristique risus.</p>
            </div>
            <div class="tab-pane fade" id="certifications">
              <h3>Certifications</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pretium pretium tempor. Ut eget imperdiet neque. In volutpat ante semper diam molestie, et aliquam erat laoreet. Sed sit amet arcu aliquet, molestie justo at, auctor nunc. Phasellus ligula ipsum, volutpat eget semper id, viverra eget nibh. Suspendisse luctus mattis cursus. Nam consectetur ante at nisl hendrerit gravida. Donec vehicula rhoncus mattis. Mauris dignissim semper mattis. Fusce porttitor a mi at suscipit. Praesent facilisis dolor sapien, vel sodales augue mollis ut. Mauris venenatis magna eu tortor posuere luctus. Aenean tincidunt turpis sed dui aliquam vehicula. Praesent nec elit non dolor consectetur tincidunt sed in felis. Donec elementum, lacus at mattis tincidunt, eros magna faucibus sem, in condimentum est augue tristique risus.</p>
            </div>
        </div>
      </div>
    </div>
  </section>

<?php
get_footer();